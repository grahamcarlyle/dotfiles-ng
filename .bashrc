# .bashrc

# Prompt colouring
export PS1="\[\033[1;36m\]\u@\h \w \$ \[\033[0m\]"

# Source other files
if [ -f /etc/bashrc ]; then
  . /etc/bashrc
fi

if [ -f $HOME/.bash_aliases ]; then
  . $HOME/.bash_aliases
fi
