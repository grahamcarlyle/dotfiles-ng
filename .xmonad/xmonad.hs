import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Layout.MultiToggle
import XMonad.Layout.Reflect
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import System.IO
import XMonad.Layout.MouseResizableTile

main = do
     xmproc <- spawnPipe "/usr/bin/xmobar /home/gcarlyle/.xmobarrc"
     xmonad $ defaultConfig {
            manageHook = manageDocks <+> manageHook defaultConfig, 
            layoutHook =  mkToggle (single REFLECTX) $ avoidStruts  $ mouseResizableTile ||| layoutHook defaultConfig,
            logHook = dynamicLogWithPP xmobarPP { 
                    ppOutput = hPutStrLn xmproc, 
                    ppTitle = xmobarColor "green" "" . shorten 125
            },
            modMask = mod4Mask
     } `additionalKeys`
       [ ((mod4Mask .|. shiftMask, xK_z), spawn "gnome-screensaver-command --lock"),
         ((mod4Mask .|. shiftMask, xK_x), sendMessage $ Toggle REFLECTX),
         ((mod4Mask .|. shiftMask, xK_u), sendMessage ShrinkSlave),
         ((mod4Mask .|. shiftMask, xK_i), sendMessage ExpandSlave),
         ((mod4Mask, xK_o), spawn "dmenu_run") ]


