# -*- mode: sh -*-
[ "$EMACS" == "t" ] || alias ls="ls --color"

alias ll="ls -l -h"
alias la="ls -a"
alias l="ls"
alias lla="ls -a -l"

alias grep="grep --color=auto"

alias f="find"
alias g="git"
alias s="ssh"
