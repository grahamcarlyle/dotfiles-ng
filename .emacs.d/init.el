(require 'package)
(add-to-list 'package-archives 
    '("marmalade" .
      "http://marmalade-repo.org/packages/"))
(package-initialize)

(defvar my-packages 
  '(starter-kit starter-kit-lisp starter-kit-bindings
		starter-kit-eshell zenburn-theme
		clojure-mode clojure-test-mode yaml-mode
		scala-mode))

(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))

(eval-after-load 'find-file-in-project
  '(add-to-list 'ffip-patterns "*.handlebars"))
