#!/bin/bash

set -x 

# Bootstrap a fresh Debian install based on my dotfiles and gems/debs lists.

set -e

ME=$1

cd /home/$ME/bin/init

echo "Installing packages via apt-get..."

apt-get install -y $(cat debs.txt)

if [ "$INSTALLTYPES" != "" ] ; then

  for INSTALLTYPE in $INSTALLTYPES; do
    apt-get install -y $(cat $INSTALLTYPE-debs.txt)

    # move this to an install type specific shell script
    if [ "$INSTALLTYPE" = "desktop" ] ; then
      cp xsession.desktop /usr/share/xsessions/xsession.desktop

      # No thank you:
      rm -rf /home/$ME/Desktop /home/$ME/Documents /home/$ME/Music /home/$ME/Pictures /home/$ME/Public \
             /home/$ME/Templates /home/$ME/Videos /home/$ME/Downloads
    fi
  done
fi

echo "Done."
