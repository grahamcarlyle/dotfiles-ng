#!/bin/bash
#
# http://j.mp/installdot

# Shamelessly ripping off technomancy
# https://github.com/technomancy/dotfiles

set -x

# Check out dotfiles and initiate bootstrap

set -e

if [ `whoami` != "root" ] ; then
  echo "You must be root."
  exit 1
fi

echo "APT::Install-Recommends \"0\";" > /etc/apt/apt.conf.d/50norecommends

apt-get update && apt-get install -y git zile

if [ "$ME" = "" ]; then
  export ME=gcarlyle
fi

if [ -z "$(getent passwd $ME)" ]; then
  echo "No user named $ME"
  exit 1
fi

if [ ! -r /home/$ME/.dotfiles ]; then
  echo "Checking out dotfiles..."
  sudo -u $ME git clone git@bitbucket.org:grahamcarlyle/dotfiles-ng.git \
    /home/$ME/.dotfiles
fi

sudo -u $ME -H sh /home/$ME/.dotfiles/bin/link-dotfiles

exec sh /home/$ME/.dotfiles/bin/init/install.sh $ME
